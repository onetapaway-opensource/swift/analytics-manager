import XCTest

import AnalyticsManagerTests

var tests = [XCTestCaseEntry]()
tests += AnalyticsManagerTests.allTests()
XCTMain(tests)
