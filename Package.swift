// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AnalyticsManager",
    platforms: [
      .iOS("12.0"),
      .macOS("10.14")
    ],
    products: [
      .library(name: "AnalyticsManager", targets: ["AnalyticsManager"]),
    ],
    dependencies: [
      .package(url: "https://gitlab.com/onetapaway-opensource/swift/emojiloghandler", from: "1.1.1")
    ],
    targets: [
      .target(name: "AnalyticsManager", dependencies: ["EmojiLogHandler"]),
      .testTarget(name: "AnalyticsManagerTests", dependencies: ["AnalyticsManager"]),
    ]
)
