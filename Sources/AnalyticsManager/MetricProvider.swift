//
//  MetricProvider.swift
//  
//
//  Created by Adam Ahrens on 4/3/20.
//

import Foundation

public protocol MetricProvider {
  var baseURL: URL { get }
  var authToken: String? { get }
  var headers: [String: String]? { get }
  func buildRequest(data: Data) -> URLRequest
}

extension MetricProvider {
  public func buildRequest(data: Data) -> URLRequest {
    var request = URLRequest(url: baseURL)
    request.httpMethod = "POST"
    request.httpBody = data
    
    // Authorized Request
    if let token = authToken {
      request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    }
    
    // Content-Type
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    // All other headers
    if let headers = headers, headers.count > 0 {
      for (key, value) in headers {
        request.setValue(value, forHTTPHeaderField: key)
      }
    }
    
    return request
  }
}
