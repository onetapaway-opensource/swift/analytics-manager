//
//  SumoMetricProvider.swift
//  
//
//  Created by Adam Ahrens on 4/8/20.
//

import Foundation

public final class SumoMetricProvider: MetricProvider {
  public var baseURL: URL
  public var authToken: String?
  public var headers: [String : String]?
  
  public init(base: URL, category: String, name: String, host: String, contentEncoding: String, contentType: String) {
    self.baseURL = base
    self.headers = ["Content-Encoding" : contentEncoding, "Content-Type" : contentType, "X-Sumo-Category" : category, "X-Sumo-Name" : name, "X-Sumo-Host" : host]
  }
}
