//
//  AuthMetricProvider.swift
//  
//
//  Created by Adam Ahrens on 4/9/20.
//

import Foundation

public final class AuthMetricProvider: MetricProvider {
  public var baseURL: URL
  public var authToken: String?
  public var headers: [String : String]?
  
  public init(base: URL, authToken: String) {
    self.baseURL = base
    self.authToken = authToken
  }
}
