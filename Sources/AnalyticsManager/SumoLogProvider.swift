//
//  SumoLogProvider.swift
//  
//
//  Created by Adam Ahrens on 4/9/20.
//

import Foundation

public final class SumoLogProvider: MetricProvider {
  public var baseURL: URL
  public var authToken: String?
  public var headers: [String : String]?
  
  public init(base: URL, category: String, name: String, host: String, contentEncoding: String) {
    self.baseURL = base
    self.headers = ["Content-Encoding" : contentEncoding, "X-Sumo-Category" : category, "X-Sumo-Name" : name, "X-Sumo-Host" : host]
  }
}
