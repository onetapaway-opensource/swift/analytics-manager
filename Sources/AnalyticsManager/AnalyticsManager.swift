import Foundation
import EmojiLogHandler

public final class AnalyticsManager {
  
  private var providers = [MetricProvider]()
  
  /// Init
  public init() {
    
  }
  
  /// Register all providers where you want metrics to flow
  public func register(providers: [MetricProvider]) {
    self.providers = providers
  }
  
  /// Send data to Metric Providers
  public func track(data: Data) {
    for provider in providers {
      let request = provider.buildRequest(data: data)
      send(request: request)
    }
  }
  
  /// Fire a list of requests. More fine grained control
  public func send(requests: [URLRequest]) {
    for request in requests {
      send(request: request)
    }
  }
  
  /// Send a URL request to a MetricProvider
  public func send(request: URLRequest) {
    URLSession.shared.dataTask(with: request) { data, response, error in
      if let httpResponse = response as? HTTPURLResponse {
        🐛("Status Code: \(httpResponse.statusCode)")
      }
      
      if let responseData = data, let response = String(data: responseData, encoding: .utf8) {
        🐛("Response: \(response)")
      }
      
      if let e = error {
        💩("Error POST event: \(e)")
      }
    }.resume()
  }
}
